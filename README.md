### Graph Coloring and Boolean Expressions Readme

This program focuses on utilizing AWK, a simple programming language widely used in Unix/Linux systems, to construct logical expressions for graph colouring and to analyze Boolean expressions in Conjunctive Normal Form (CNF).

#### Problems Overview:

1. **Problems 1–4:** Constructing an AWK program to generate Boolean expressions in CNF representing 3-colorability of graphs.
   - **Problem 1:** Constructing a Boolean expression for a specific graph.
   - **Problem 2:** Writing an AWK script to generate Boolean expressions for any graph.
   - **Problem 3:** Providing an expression for the number of clauses in terms of graph properties.
   - **Problem 4:** Modifying the AWK script to output commands for testing satisfiability using SageMath.

2. **Problem 5:** Apply induction to prove properties of slithy Boolean expressions in CNF.

#### File Organization:

- `prob1.txt`: Contains the Boolean expression for the specific graph in CNF format.
- `prob2.awk`: AWK script to generate Boolean expressions for any input graph.
- `prob3.pdf`: PDF file explaining the formula for the number of clauses.
- `prob4.awk`: Modified AWK script to output commands for satisfiability testing.
- `README.md`: This README file provides an overview of the programs and instructions for solutions.

#### Instructions for Testing:

1. Execute `prob2.awk` on an input file containing a graph to generate Boolean expressions.
2. Run SageMath commands generated by `prob4.awk` on the generated Boolean expressions to test satisfiability.
3. Review `prob1.txt`, `prob3.pdf`, and `prob5.md` for specific solutions and explanations.
