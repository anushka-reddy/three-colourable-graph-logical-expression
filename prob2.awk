#!/usr/bin/awk -f

BEGIN {
    FS = " -- "
    
}

{
    if (NR == 1) {
        numVertices = $1;
        generateColorConstraints(numVertices);
    } else {
        generateNoSameColorConstraints($1, $2);
    }
}

END {
    generateCompletenessConstraints(numVertices);
}

function generateColorConstraints(vertices) {
    for (v = 1; v <= vertices; v++) {
        for (color in colors) {
            clause = clause varName(v, color) " | ";
        }
        sub(" \\| $", "", clause);
        printf "(" clause ") & ";
    }
}

function generateNoSameColorConstraints(v1, v2) {
    for (color in colors) {
        clause = clause "(~" varName(v1, color) " | ~" varName(v2, color) ") & ";
    }
    sub(" & $", "", clause);
    printf "(" clause ") & ";
}

function generateCompletenessConstraints(vertices) {
    for (v = 1; v <= vertices; v++) {
        for (color in colors) {
            clause = clause varName(v, color) " | ";
        }
        sub(" \\| $", "", clause);
        printf "(" clause ")";
        if (v < vertices) {
            printf " & ";
        }
    }
}

function varName(vertex, color) {
    return "v" vertex color;
}

BEGIN {
    colors["Red"] = 1;
    colors["White"] = 1;
    colors["Black"] = 1;
}
